import React, {Component} from 'react'
import P5Wrapper from 'react-p5-wrapper';
import sketch from "./sketch"
function TicTacToe() {
    return(
        <P5Wrapper sketch={sketch}></P5Wrapper>
    );
}

export default TicTacToe;