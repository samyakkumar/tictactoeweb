import React, {Component} from 'react';

class Home extends Component{
    constructor(props) {
        super(props)
    }
    handleClick(){
            fetch("http://127.0.0.1:5000/makeGame", {
                method: "POST", 
                redirect: "follow"
            })
            window.location = "http://127.0.0.1:5000/ticTacToe"
    }
    render() {
        return(
            <button onClick={this.handleClick}>TicTacToe</button>
        )
    }
}

export default Home;