
export default function sketch(p) {
    let canvas;
    let board = new Board(p);
    
    p.setup = () => {
        canvas = p.createCanvas(400, 400);
        console.log(board)
    }

    p.draw = () => {
        p.background(200, 200, 200);
        board.show()
    }
    p.mouseClicked = () => {
        board.sendClick(p.mouseX, p.mouseY);
    }
}

class Board {
    constructor(p){
        this.board = [
            ["", "", ""],
            ["", "", ""],
            ["", "", ""]
        ];
        this.player = "X"
        this.otherPlayer = "O"
        this.playerChance = true
        this.p = p;
        this.cellWidth = 40;
        this.cellHeight = 40;
        this.cells = []
        var y = 140;
        this.nextI = 0;
        for (var i = 0; i < this.board.length; i++) {
            var x = 140;
            for (var j = 0; j < this.board[i].length; j++) {
                this.cells.push(new Cell(this.p, x, y, this.cellWidth, this.cellHeight));
                x += this.cellWidth;
            }
            y += this.cellHeight;
        }
    }
    sendClick(x, y) {
        console.log(x, y)
        for (var i = 0; i < this.cells.length; i++) {
            if (this.cells[i].checkClick(x, y)) {
                var boardY = i % 3
                var boardX = Math.trunc(i / 3)
                console.log(i)
                console.log("//////////")
                console.log(boardX)
                console.log(boardY)
                if (this.playerChance) {
                    this.cells[i].setText(this.player)
                    this.board[boardX][boardY] = this.player
                    this.playerChance = !this.playerChance
                }
                fetch("http://127.0.0.1:5000/updateBoard", {
                    method: "POST",
                    headers: {
                        "Content-Type" : "application/json",
                    }, 
                    body: JSON.stringify({
                        "i" : i, 
                        "board": this.board,
                        "playerChance": this.playerChance
                    })
                }).then(res => res.json()).then(data => {
                    this.nextI = data["aiPlay"]
                    console.log("next I")
                    console.log(this.nextI)
                    if (!this.playerChance && this.nextI != []) {
                        var otherBoardY = this.nextI[1]
                        var otherBoardX = this.nextI[0]
                        data.board[otherBoardX][otherBoardY] = this.otherPlayer
                        this.playerChance = !this.playerChance
                    }
                    this.board = data.board

                    console.log(data)
                    console.log(this.board)
                    y = 140
                    this.cells = []
                    
                    for (var k = 0; k < this.board.length; k++) {
                        var x = 140;
                        for (var j = 0; j < this.board[k].length; j++) {
                            var newCell = new Cell(this.p, x, y, this.cellWidth, this.cellHeight)
                            newCell.setText(this.board[k][j])
                            this.cells.push(newCell);
                            x += this.cellWidth;
                        }
                        y += this.cellHeight;
                    }
                    console.log(this.cells)
                    
                })
            }
        }
    }

    show() {
        for (var i = 0; i < this.cells.length; i++) {
            this.cells[i].show()
        }
    }
}

class Cell {
    constructor(p,  x, y, cellWidth, cellHeight) {
        this.cellHeight = cellHeight;
        this.cellWidth = cellWidth;
        this.p = p;
        this.x = x;
        this.y = y;
        this.text = ""
    }
    setText(t) {
        this.text = t;
    }
    checkClick(x, y) {
        return ((x >= this.x && x <= this.x + this.cellWidth) 
                    && y >= this.y && y <= this.y + this.cellHeight)
    }
    show() {
        this.p.rect(this.x, this.y, this.cellWidth, this.cellHeight);
        if (this.text != "") {
            this.p.textAlign(this.p.CENTER);
            this.p.text(this.text, this.x + this.cellWidth / 2, this.y + this.cellHeight / 2)
        }
    }
}
