// App.jsx
import React, {Component} from "react";
import { BrowserRouter as Router, Route } from "react-router-dom"; 
import Home from './Home'
import TicTacToe from "./TicTacToe"
function App() {
      return (
        <Router>
        <Route exact path="/"  component = {Home}/>
        <Route path="/ticTacToe" component = {TicTacToe}/>
      </Router>
      )
}
export default App;