# server.py
from flask import Flask, render_template, request, redirect
from Game import Game
import json
import random
import copy
import math
app = Flask(__name__, static_folder="../static/dist", template_folder="../static")
newGame = None

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def hello(path):
    return render_template("index.html")

@app.route("/makeGame", methods=["POST"])
def makeGame():
    global newGame
    print('here')
    if request.method == "POST":
        return redirect("ticTacToe")

@app.route("/updateBoard", methods=["POST"])
def updateBoard():
    global newGame
    if request.method == "POST":
        req = request.json
        i = req["i"]
        board = req["board"]
        player_chance = req["playerChance"]
        print(i)
        print(board)
        bestMove = None
        bestVal = -10000
        for coord in getAvailableSpots(board):
            newState = getNewState(board, coord, player_chance)
            val = minimax(newState, 1000, player_chance)
            if (val > bestVal):
                bestVal = val
                bestMove = coord
        res = {
            "aiPlay": bestMove,
            "board" : board,
            "winner": isGameOver(board)[0]
        }
        return json.dumps(res)

def getAvailableSpots(board):
    availSpots = []
    for row in range(len(board)):
        for col in range(len(board[0])):
            if (board[row][col] == ""):
                availSpots.append((row, col))
    return availSpots

def getNewState(board, move, isPlayerChance):
    x, y = move
    newBoard = copy.deepcopy(board)
    newBoard[x][y] = "X" if isPlayerChance else "O"
    return newBoard

def isGameOver(board):
    # diagonals check
    if ((board[0][0] != "" and board[1][1] != "" and board[2][2] != "" ) and (board[0][0] == board[1][1] == board[2][2]) 
        or ((board[0][2] != "" and board[1][1] != "" and board[2][0] != "") and (board[0][2] == board[1][1] == board[2][0]))):
        return (True, board[1][1])
    # col check
    for row in range(len(board)):
        if ((board[0][row] != "" and board[1][row] != "" and board[2][row] != "") and (board[0][row] == board[1][row] == board[2][row])):
            return (True, board[0][row])
    # row check
    for col in range(len(board)):
        if ((board[col][0] != "" and board[col][1] != "" and board[col][2] != "") and (board[col][0] == board[col][1] == board[col][2] != "")):
            return (True, board[col][0])
    return (False, None)

def score(board):
    isWinner = isGameOver(board)[1]
    if (not isWinner):
        return 0
    elif (isWinner == "X"):
        return -10
    elif (isWinner == "O"):
        return 10


def minimax(board, depth, isPlayerChance):
    if (isGameOver(board)[0]):
        return score(board)

    if (not isPlayerChance):
        value = -math.inf
        for coord in getAvailableSpots(board):
            value = max(value, minimax(getNewState(board, coord, not isPlayerChance), depth - 1, not isPlayerChance))
        return value
    else:
        value = math.inf
        for coord in getAvailableSpots(board):
            value = min(value,  minimax(getNewState(board, coord, not isPlayerChance), depth - 1, not isPlayerChance))
        return value

if __name__ == "__main__":
    app.run()